Microcontent Revision UI

This module takes advantage of work in progress Drupal core patches to add
generic revision UI to Microcontent.

# Instructions

 1. Requires core patches. See *Patches* section below
 2. Assign appropriate user permissions.

# Patches

## Adds generic revision UI

https://www.drupal.org/node/2350939

Adds the ability for entities other than node to use a revision UI. Including
revert form, and history list.
